-- my sql advance queries and joins
-- [section] add new records
-- add 5 artists, atleast 2 albums each, 1-2 songs per album

INSERT INTO artists (name) VALUES("Taylor Swift");
INSERT INTO artists (name) VALUES("Lady Gaga");
INSERT INTO artists (name) VALUES("TJustin Bieber");
INSERT INTO artists (name) VALUES("Ariana Grande");
INSERT INTO artists (name) VALUES("Bruno Mars");

INSERT INTO albums (album_title, date_release, artist_id) VALUES("Fearless", "2008-11-11",3);
INSERT INTO albums (album_title, date_release, artist_id) VALUES("Red", "2012-10-22",3);

INSERT INTO albums (album_title, date_release, artist_id) VALUES("A Star Is Born", "2018-10-05",4);
INSERT INTO albums (album_title, date_release, artist_id) VALUES("Born This Way", "2011-05-23",4);

INSERT INTO albums (album_title, date_release, artist_id) VALUES("Purpose", "2015-11-13",5);
INSERT INTO albums (album_title, date_release, artist_id) VALUES("Believe", "2012-06-15",5);

INSERT INTO albums (album_title, date_release, artist_id) VALUES("Dangerous Woman", "2016-05-20",6);
INSERT INTO albums (album_title, date_release, artist_id) VALUES("Thank U", "2019-02-08",6);

INSERT INTO albums (album_title, date_release, artist_id) VALUES("24k Magic", "2016-11-18",7);
INSERT INTO albums (album_title, date_release, artist_id) VALUES("Earth to Mars", "2011-02-07",7);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 402, "Pop Rock",4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 355, "Country Pop",4);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 455, "Rock, Alternative Rock, Arena Rock",5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 341, "Country",5);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 304, "Rock and Roll" ,6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 336, "Country, Rock, Folk Rock",6);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 420, "Electropop",7);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 320, "Dancehall-poptropical Housemoombahton",8);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 252, "Pop",9);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 405, "EDM House",10);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U, Next", 327, "Pop, R&B",11);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24k Magic", 346, "Funk, Disco, R&B",12);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 321, "Pop",13);

-- Exclude records
SELECT * FROM songs WHERE id != 11;

-- LIMIT show only specific number of records.
SELECT * FROM songs LIMIT 5;

-- Greater than or equal
SELECT * FROM songs WHERE id <= 11;
SELECT * FROM songs WHERE id >= 11;

-- Select specific records using OR
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 9;

-- Get specific records using IN
-- a shorthand method for multiple OR conditions.
SELECT * FROM songs WHERE id IN (1,5,9);

SELECT * FROM songs WHERE genre IN ("Pop","K-Pop");

-- Combining conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

-- Find partial matches
-- LIKE is used in a WHERE clause to search for a specified pattern in a column
-- There are two wildcards used in conjunction with LIKE
-- % - represents zero, one or multiple characters
-- _ - represents a single character ex. 2012-__-__

SELECT * FROM songs WHERE song_name LIKE "%e";
-- select keywords from the end

SELECT * FROM songs WHERE song_name LIKE "b%";
-- select keywords from the start

SELECT * FROM songs WHERE song_name LIKE "%a%";
-- select keyword anywhere

SELECT * FROM albums WHERE date_released LIKE "____-__-__";
-- get a record based on a specific pattern.

-- sorting records (alphanumeric order: A-Z/Z-A)
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

SELECT * FROM songs ORDER BY song_name ASC LIMIT 5;
SELECT * FROM songs ORDER BY song_name DESC LIMIT 6;

-- getting distinct records.
-- eliminates duplicate rows and display a 
SELECT DISTINCT genre FROM songs;

-- Table Joins
-- This is used to retrieve data from multiple tables.
-- this is performed whenever two or more tables are listed in a sql statement
-- https://joins.spathon.com/

-- syntax of inner join(join)
-- two tables: 
-- SELECT * (column_name) FROM left_table_name JOIN right_table_name ON left_column_id_pk/fk = right_column_id_fk;

-- multiple tables:
-- SELECT * (column_name) FROM table1_name JOIN table2_name ON table1_column_id_pk/fk = table2_column_id_fk JOIN table3_name ON table2_column_id_pk/fk = table3_column_id_pk/fk;


-- combine artists and albums table.
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id;

-- combine more than two tables
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_idS

-- specify columns to be included in the join table.
SELECT artists.name, albums.album_title, songs.song_name FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;

-- insert another artist
INSERT INTO artists (name) VALUE ("Ed Sheeran");

-- show artists without records on the right side of the joined table.
SELECT artists.name, albums.album_title FROM artists
    LEFT JOIN albums ON artists.id = albums.artist_id;

-- right join
SELECT artists.name, albums.album_title FROM artists
    RIGHT JOIN albums ON artists.id = albums.artist_id;

-- full outer join
-- UNION: is used to combine the result set of two or more SELECT statements;
SELECT artists.name, albums.album_title FROM albums
    LEFT JOIN artists ON albums.artist_id = albums.id
    UNION
SELECT artists.name, albums.album_title FROM albums
    RIGHT JOIN artists ON albums.artist_id = albums.id;
